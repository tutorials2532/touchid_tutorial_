//
//  AppDelegate.h
//  TouchId_tutorial
//
//  Created by Ali Farhan on 06/01/2015.
//  Copyright (c) 2015 Ali Farhan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

