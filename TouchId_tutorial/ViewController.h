//
//  ViewController.h
//  TouchId_tutorial
//
//  Created by Ali Farhan on 06/01/2015.
//  Copyright (c) 2015 Ali Farhan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>
@interface ViewController : UIViewController
- (IBAction)authenticateButtonTapped:(id)sender;

@end

